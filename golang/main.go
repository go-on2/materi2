package main

import (
	"fmt"
	_package "gitlab.com/go-on2/materi2/package"
	"sync"
	"time"
)

const conferenceTiket int = 50

var conferenceName = "Go Conference"
var remainingTiket uint = 50
var bookings = make([]UserData, 0)

type UserData struct {
	firstName     string
	lastName      string
	email         string
	numberOfTiket uint
}

var wg = sync.WaitGroup{}

func main() {

	greetUsers()

	firstName, lastName, email, userTiket := getUserInput()
	isValidName, isValidEmail, isValidTiketNumber := _package.ValidateUserInput(firstName, lastName, email, userTiket, remainingTiket)

	if isValidName && isValidEmail && isValidTiketNumber {

		bookTicket(userTiket, firstName, lastName, email)

		wg.Add(1)
		go sendTicket(userTiket, firstName, lastName, email)

		firstNames := getFirstNames()
		fmt.Printf("The first names of bookings are: %v\n", firstNames)

		if remainingTiket == 0 {
			fmt.Println("Our conference is booked out. Come back next year.")
		}
	} else {
		if !isValidName {
			fmt.Println("first name or last name you entered is too short")
		}
		if !isValidEmail {
			fmt.Println("email address you entered doesn't contain @ sign")
		}
		if !isValidTiketNumber {
			fmt.Println("number of tickets you entered is invalid")
		}
	}
	wg.Wait()
}

func greetUsers() {
	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTiket, remainingTiket)
	fmt.Println("Get your tickets here to attend")
}

func getFirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames
}

func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTiket uint

	fmt.Println("Enter your first name: ")
	fmt.Scan(&firstName)

	fmt.Println("Enter your last name: ")
	fmt.Scan(&lastName)

	fmt.Println("Enter your email address: ")
	fmt.Scan(&email)

	fmt.Println("Enter number of tickets: ")
	fmt.Scan(&userTiket)

	return firstName, lastName, email, userTiket
}

func bookTicket(userTiket uint, firstName string, lastName string, email string) {
	remainingTiket = remainingTiket - userTiket

	var userData = UserData{
		firstName:     firstName,
		lastName:      lastName,
		email:         email,
		numberOfTiket: userTiket,
	}

	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)

	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTiket, email)
	fmt.Printf("%v tickets remaining for %v\n", remainingTiket, conferenceName)
}

func sendTicket(userTiket uint, firstName string, lastName string, email string) {
	time.Sleep(15 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTiket, firstName, lastName)
	fmt.Println("#################")
	fmt.Printf("Sending ticket:\n %v \nto email address %v\n", ticket, email)
	fmt.Println("#################")
	wg.Done()
}
