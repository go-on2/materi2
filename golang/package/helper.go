package _package

import "strings"

func ValidateUserInput(firstName string, lastName string, email string, userTiket uint, remainingTiket uint) (bool, bool, bool) {
	isValidName := len(firstName) >= 2 && len(lastName) >= 2
	isValidEmail := strings.Contains(email, "@")
	isValidTiketNumber := userTiket > 0 && userTiket <= remainingTiket
	return isValidName, isValidEmail, isValidTiketNumber
}
